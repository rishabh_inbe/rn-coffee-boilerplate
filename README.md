# React-Native-CoffeeScript Boilerplate

## Steps to Work on this Boilerplate

### 1. Clone the Repo

```
git clone https://rishabh_inbe@gitlab.com/rishabh_inbe/rn-coffee-boilerplate.git 
cd rn-coffee-boilerplate
```

### 2. Install Node Modules

```
npm install
```

### 3. Run Gulp

```
npm run-script build
```

### 4. Modifiy app.coffee in `coffee` folder


#### All coffee files should be in `coffee` folder and all converted JS files will output to `js` folder