const gulp = require('gulp')
const sourcemaps = require('gulp-sourcemaps')
const cjsx = require('gulp-cjsx')

gulp.task('coffee',()=>{
    gulp.src('coffee/**/*.coffee')
        .pipe(sourcemaps.init())
        .pipe(cjsx())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('js'))

})

gulp.task('default', () => {
    gulp.watch('coffee/**/*.coffee', ['coffee'])
})